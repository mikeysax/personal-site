module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Michael Farrell',
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content:
          'width=device-width initial-scale=1 maximum-scale=1 user-scalable=no minimal-ui'
      },
      {
        hid: 'description',
        name: 'description',
        content: 'Michael Farrell - Web Developer And Engineer'
      },
      {
        property: 'og:image',
        content: 'https://avatars2.githubusercontent.com/u/15805689?s=460&v=4'
      },
      {
        property: 'og:description',
        content: 'Michael Farrell - Web Developer And Engineer'
      },
      {
        property: 'og:title',
        content: 'Michael Farrell'
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    script: [{ src: 'https://use.fontawesome.com/2778da33c5.js' }]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  modules: ['@nuxtjs/sitemap'],
  sitemap: {
    path: '/sitemap.xml',
    hostname: 'https://mikeysax.com',
    cacheTime: 1000 * 60 * 15,
    generate: true, // Enable me when using nuxt generate
    routes: [
      '/',
      {
        url: '/portfolio',
        changefreq: 'daily',
        priority: 1,
        lastmodISO: '2017-06-30T13:30:00.000Z'
      }
    ]
  },
  build: {
    /*
    ** Run ESLint on save
    */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        // config.module.rules.push({
        //   enforce: 'pre',
        //   test: /\.(js|vue)$/,
        //   loader: 'eslint-loader',
        //   exclude: /(node_modules)/
        // });
      }
    }
  }
};
